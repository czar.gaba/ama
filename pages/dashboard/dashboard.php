<?php 
session_start();
ob_start();
    include "../header.php"; 
?>
<?php include "../sidebar.php"; ?>
<?php include "../connection.php"; ?>
             
            <!-- START CONTENT -->
            <section class="content">
            <?php include "../modals/account_modal.php"; ?>
                <div class="container-fluid ">
                    <div class="block-header">
                        <h2>DASHBOARD</h2>
                    </div>

                    <!-- Widgets -->
                    <div class="row clearfix">

                    <?php
                        if($_SESSION['role'] == "Administrator"){
                    ?>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">person</i>
                                </div>
                                <div class="content">
                                    <div class="text">INSTRUCTORS</div>
                                    <div class="number count-to">
                                        <?php
                                            include "../connection.php";

                                            $q = mysqli_query($con,"SELECT * from tblfaculty");
                                            $ct = mysqli_num_rows($q);

                                            echo $ct;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">person</i>
                                </div>
                                <div class="content">
                                    <div class="text">STUDENTS</div>
                                    <div class="number count-to">
                                        <?php
                                            include "../connection.php";

                                            $q = mysqli_query($con,"SELECT * from tblstudent");
                                            $ct = mysqli_num_rows($q);

                                            echo $ct;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">near_me</i>
                                </div>
                                <div class="content">
                                    <div class="text">FILES SUBMITTED</div>
                                    <div class="number count-to">
                                        <?php
                                           

                                            $q1 = mysqli_query($con,"SELECT * from tblfilessubmitted");
                                            $ct1 = mysqli_num_rows($q1);

                                            echo $ct1;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">check</i>
                                </div>
                                <div class="content">
                                    <div class="text">APPROVED FILES</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tblfilessubmitted where status = 'Approved' ");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment</i>
                                </div>
                                <div class="content">
                                    <div class="text">STUDENT ACTIVITIES</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tblstudactivities");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                           <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment</i>
                                </div>
                                <div class="content">
                                    <div class="text">FACULTY ACTIVITIES</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tblactivities");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>                     
                    <!-- Faculty -->
                    <?php
                    }
                    else if ($_SESSION['role'] == "Faculty")
                    {
                    ?>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">near_me</i>
                                </div>
                                <div class="content">
                                    <div class="text">FILES SUBMITTED</div>
                                    <div class="number count-to">
                                        <?php
                                           

                                            $q1 = mysqli_query($con,"SELECT * from tblfilessubmitted where facultyid = '".$_SESSION['userid']."' ");
                                            $ct1 = mysqli_num_rows($q1);

                                            echo $ct1;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">check</i>
                                </div>
                                <div class="content">
                                    <div class="text">APPROVED FILES</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tblfilessubmitted where status = 'Approved' and  facultyid = '".$_SESSION['userid']."' ");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">attach_file</i>
                                </div>
                                <div class="content">
                                    <div class="text">MY FILES</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tblteacherfile where facultyid = '".$_SESSION['userid']."' ");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">file_download</i>
                                </div>
                                <div class="content">
                                    <div class="text">DOWNLOADABLE FILES</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tbldownloadable");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">notifications_active</i>
                                </div>
                                <div class="content">
                                    <div class="text">NOTIFICATIONS</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tblnotification where facultyid = '".$_SESSION['userid']."' ");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment</i>
                                </div>
                                <div class="content">
                                    <div class="text">ACTIVITY</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tblactivities");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    <!-- Student -->
                    <?php
                    }
                    else
                    {
                    ?>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">check</i>
                                </div>
                                <div class="content">
                                    <div class="text">MY FILES</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tblsudentfile where studentid = '".$_SESSION['userid']."' ");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="content">
                                    <div class="text">DOWNLOADABLE FILES</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tbldownloadable");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">check</i>
                                </div>
                                <div class="content">
                                    <div class="text">ACTIVITY</div>
                                    <div class="number count-to">
                                        <?php

                                            $q2 = mysqli_query($con,"SELECT * from tblstudactivities");
                                            $ct2 = mysqli_num_rows($q2);

                                            echo $ct2;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>                
                    <?php
                    }
                    ?>
                    
                    </div>
                </div>

                <!--end container-->
                <?php include "../notification/notification.php"; ?> 
            

            </section>
            <!-- END CONTENT -->

            
        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->



    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <?php include "../footer.php"; ?>

    <script>
        $(document).ready(function() {
            $('#dttbl').DataTable( {
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0,2 ] } ],"aaSorting": []
            } );
        } ); 
    </script>
         